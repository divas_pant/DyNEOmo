module coord_trans
    use constants
    use crossprod
    use ephimeris
    use kepler_solver

    implicit none

    contains

    subroutine rv2orbparam(r_in, v_in, param_out)
        !*****************************************************************************************
        !*****************************************************************************************
        ! Convert r&v to standard kepler parameters **********************************************
        !*****************************************************************************************
        !*****************************************************************************************
        !
        ! Written       :: Divas based on Howard Curtis
        ! Created       :: Jan 2017
        ! Last updated  :: Jan 2017
        !
        ! Vars          :: r_in array of radius vector
        !                  v_in array of velocity vector
        !                  param_out kepler parameters arranged as
        !                  (sma, ecc, arg of perigee, RA of ascending node, true anomaly)
        !
        !*****************************************************************************************
        !*****************************************************************************************
        integer, parameter                  :: dp=kind(0.d0)
        real(dp), parameter, dimension(3)   :: I_vec = (/0.0, 0.0, 1.0/)
        real(dp), intent(IN), dimension(3)  :: r_in, v_in
        real(dp), dimension(6)              :: param_out
        real(dp)                            :: eps, r_norm, v_norm, v_radial, H_norm, Ecc_norm, &
                                               inclination, omega_norm, omegac_norm, n_norm, T_A, sma
        real(dp), dimension(3)              :: H_vec, N_vec, ecc_vec, cp

        eps = 1e-10_dp
        r_norm = norm2(r_in);   v_norm = norm2(v_in)
        v_radial = dot_product(r_in, v_in)/r_norm

        h_vec = cross_product(r_in, v_in)
        h_norm = norm2(h_vec)

        inclination = acos(H_vec(3)/H_norm)

        n_vec = cross_product(I_vec, H_vec)
        n_norm = norm2(N_vec)

        if (n_norm /= 0.0) then
            omegac_norm = acos(n_vec(1)/n_norm)
            if (N_vec(2) < 0.0) then
                omegac_norm = 2*pi-omegac_norm
            end if
        else
            omegac_norm = 0
        end if

        ecc_vec = 1/mu*((v_norm**2-mu/r_norm)*r_in - r_norm*v_radial*v_in)
        ecc_norm = norm2(ecc_vec)

        if (n_norm /= 0.0) then
            if (Ecc_norm > eps) then
                omega_norm = acos(dot_product(N_vec, ecc_vec)/n_norm/Ecc_norm)
                if (ecc_vec(3) < 0.0) then
                    omega_norm = 2*pi - omegac_norm
                end if
            else
                omega_norm = 0.0
            end if
        else
            omega_norm = 0.0
        end if

        if (Ecc_norm > eps) then
            T_A = acos(dot_product(ecc_vec, r_in)/ecc_norm/r_norm)
            if (v_radial < 0.0) then
                T_A = 2*pi-T_A
            end if
        else
            cp = cross_product(N_vec, r_in)
            if (cp(3) >= 0) then
                T_A = acos(dot_product(N_vec, r_in)/n_norm/r_norm)
            else
                T_A = 2*pi - acos(dot_product(N_vec, r_in)/n_norm/r_norm)
            end if
        end if

        sma = H_norm**2/mu/(1-Ecc_norm**2)

        param_out = (/sma, Ecc_norm, inclination, omega_norm, omegac_norm, T_A/)

    end subroutine


    subroutine orbparam2rv(param_in, r_out, v_out)
        !*****************************************************************************************
        !*****************************************************************************************
        ! Convert keplerian parameters to r&v ****************************************************
        !*****************************************************************************************
        !*****************************************************************************************
        !
        ! Written       :: Divas
        ! Created       :: Jan 2017
        ! Last updated  :: Jan 2017
        !
        ! Vars          :: param_in array of standard parameter
        !                  (sma, ecc, arg of perigee, RA of ascending node, true anomaly)
        !                  param_out kepler parameters arranged as
        !
        !
        !*****************************************************************************************
        !*****************************************************************************************
        integer, parameter                      :: dp=kind(0.d0)
        real(dp), intent(in), dimension(6)      :: param_in
        real(dp)                                :: sma, ecc, inclination, omega, omegaC, theta, h
        real(dp),DIMENSION(3)                   :: r_out, v_out
        real(dp),DIMENSION(3,3)                 :: R3_OmegaC, R1_i, R3_omega, Qpx

        sma   = param_in(1);     ecc    = param_in(2);      inclination = param_in(3)
        omega = param_in(4);     omegaC = param_in(5);      theta       = param_in(6)

        h = sqrt(sma*mu*(1-ecc**2))

        r_out(1) = cos(theta)*((h**2)/mu)/(1 + ecc*cos(theta));    r_out(2) = sin(theta)*((h**2)/mu)/(1 + ecc*cos(theta))
        r_out(3) = 0.0_dp

        v_out(1) = -(mu/h) *sin(theta);    v_out(2) = (mu/h) *(ecc+cos(theta));    v_out(3) = 0.0_dp

        R3_OmegaC(1,1) = cos(OmegaC);       R3_OmegaC(1,2) = sin(OmegaC);          R3_OmegaC(1,3) = 0.0_dp
        R3_OmegaC(2,1) = -sin(Omegac);      R3_OmegaC(2,2) = cos(OmegaC);          R3_OmegaC(2,3) = 0.0_dp
        R3_OmegaC(3,1) = 0.0_dp;            R3_OmegaC(3,2) = 0.0_dp;               R3_OmegaC(3,3) = 1.0_dp

        R1_i(1,1) = 1.0_dp;                 R1_i(1,2) = 0.0_dp;                    R1_i(1,3) = 0.0_dp
        R1_i(2,1) = 0.0_dp;                 R1_i(2,2) = cos(inclination);          R1_i(2,3) = sin(inclination)
        R1_i(3,1) = 0.0_dp;                 R1_i(3,2) = -sin(inclination);         R1_i(3,3) = cos(inclination)

        R3_omega(1,1) = cos(omega);         R3_omega(1,2) = sin(omega);            R3_omega(1,3) = 0.0_dp
        R3_omega(2,1) = -sin(omega);        R3_omega(2,2) = cos(omega);            R3_omega(2,3) = 0.0_dp
        R3_omega(3,1) = 0.0_dp;             R3_omega(3,2) = 0.0_dp;                R3_omega(3,3) = 1.0_dp

        qpx = matmul(transpose(R3_OmegaC),transpose(R1_i))
        qpx = matmul(qpx,transpose(R3_omega))

        r_out = matmul(qpx, r_out)
        v_out = matmul(qpx, v_out)

    end subroutine

    function eq2ecl(x_in)
        integer, parameter                  :: dp=kind(0.d0)
        real(dp), dimension(3), intent(in)  :: x_in
        real(dp), dimension(3)              :: eq2ecl
        real(dp), dimension(3,3)            :: R_rot
        integer                             :: i, j

        R_rot = reshape((/1.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, cos(inc), sin(inc), 0.0_dp, -sin(inc), cos(inc)/),&
                        (/3,3/), order = (/1,2/))


         R_rot(1,1) = 1.0_dp;           R_rot(1,2) = 0.0_dp
         R_rot(1,3) = 0.0_dp;           R_rot(2,1) = 0.0_dp
         R_rot(2,2) = cos(inc);         R_rot(2,3) = sin(inc)
         R_rot(3,1) = 0.0_dp;           R_rot(3,2) = -sin(inc)
         R_rot(3,3) = cos(inc)

         do i = 1, 3
            eq2ecl(i) = 0
            do j = 1, 3
                eq2ecl(i) = eq2ecl(i)+R_rot(i,j)*x_in(j)
            end do
         end do


    end function

    subroutine MEE2BCE(x_in, t_in, x_out)
        !*****************************************************************************************
        !*****************************************************************************************
        ! Convert Equinoctial elements to Barrycentric ecliptic parameters to r&v ****************
        !*****************************************************************************************
        !*****************************************************************************************
        !
        ! Written       :: Divas
        ! Created       :: Jan 2017
        ! Last updated  :: Jan 2017
        !
        ! Vars          :: x_in array of equinoctial elements (a, P1, P2, Q1, Q2, L)
        !                  t_in time for coordinate data available, needed to get position of sun
        !                  x_out array of r and v in BCE
        !
        !*****************************************************************************************
        !*****************************************************************************************

        integer, parameter                  :: dp=kind(0.d0)
        real(dp), dimension(6), intent(in)  :: x_in
        real(dp), dimension(6)              :: x_out, rv_param
        real(dp)                            :: sma, p1, p2, q1, q2, ml, k, f, fp, e2, rm, ab, sinl, &
                                               cosl, dum, h, p, t_in, order, mu
        integer                             :: i, j
        real(dp), dimension(3,3)            :: AA
        real(dp), dimension(3)              :: r, v, rr, vv

        order = 2;                          mu = 0.2959122082856201E-003
        sma = x_in(1);                      p1 = x_in(2)
        p2  = x_in(3);                      q1 = x_in(4)
        q2  = x_in(5);                      ml = x_in(6)

        k = ml
        f = k+p1*COS(K)-p2*SIN(K)-ml
        do WHILE (ABS(f)>1e-14)
           f  = k+p1*COS(k)-p2*SIN(k)-ml
           fp = 1-P1*SIN(K)-P2*COS(K)
           k  = k-f/fp
        end do

        f = k+p1*COS(k)-p2*SIN(k)-ml

        do i = 1, INT(LOG(order+1)/LOG(2.0))+1;
           f  = k+p1*COS(k)-p2*SIN(k)-ml;
           fp = 1-p1*SIN(k)-p2*COS(k)
           k  = k-f/fp;
        end do

        e2   = P1**2 + P2**2
        p    = sma*(1-e2)
        h    = SQRT(p * mu)
        rm   = sma*(1-(p1*SIN(k))-(p2*COS(k)))
        ab   = 1/(1+sqrt(1-P1**2-P2**2))
        sinl = sma/rm*((1-ab*p2**2)*SIN(k) + ab*p1*p2*COS(k)-p1)
        cosl = sma/rm*((1-ab*p1**2)*COS(k) + ab*p1*p2*SIN(k)-p2)


        r(1) = rm*cosl
        r(2) = rm*sinl
        r(3) = 0
        v(1) = h/p*(-p1-sinl)
        v(2) = h/p*(p2+cosl)
        v(3) = 0

!       r and v in inertial Sun centered ecliptic
        dum     = 1/(1+Q1**2+Q2**2);
        AA(1,1) = dum*(1-Q1**2+Q2**2);       AA(1,2) = dum*2*Q1*Q2;
        AA(1,3) = dum*2*Q1;                  AA(2,1) = dum*2*Q1*Q2;
        AA(2,2) = dum*(1+Q1**2-Q2**2);       AA(2,3) = dum*(-2*Q2**2);
        AA(3,1) = dum*(-2*Q1);               AA(3,2) = dum*2*Q2;
        AA(3,3) = dum*(1-Q1**2-Q2**2)

        do i = 1, 3
            rr(i) = 0
            vv(i) = 0
            do j = 1, 3
                rr(i) = rr(i)+aa(i,j)*r(j)
                vv(i) = vv(i)+aa(i,j)*v(j)
            end do
        end do

!       Sun position in Solar system barycenter equatorial J2000
        call PLEPH(t_in, 11, 12, rv_param)

!       Sun position in Solar system barycenter ecliptic
        r(1) =  rv_param(1)
        r(2) =  rv_param(2)*cos(inc) + rv_param(3)*sin(inc);
        r(3) = -rv_param(2)*sin(inc) + rv_param(3)*cos(inc);
        v(1) =  rv_param(4)
        v(2) =  rv_param(5)*cos(inc) + rv_param(6)*sin(inc);
        v(3) = -rv_param(5)*sin(inc) + rv_param(6)*cos(inc);

!       Asteroid position in Solar system barycenter ecliptic
        do i = 1, 3
           x_out(i)   = r(i) + rr(i)
           x_out(i+3) = v(i) + vv(i)
        end do

    end subroutine

    subroutine MEE2KEP(param1, x_out)
        !*****************************************************************************************
        !*****************************************************************************************
        ! Convert Equinoctial elements to Keplerian parameters ***********************************
        !*****************************************************************************************
        !*****************************************************************************************
        !
        ! Written       :: Divas
        ! Created       :: Jan 2017
        ! Last updated  :: Jan 2017
        !
        ! Vars          :: param1 array of equinoctial elements
        !                  x_out kepler parameters
        !
        !
        !*****************************************************************************************
        !*****************************************************************************************
        integer, parameter                  :: dp=kind(0.d0)
        real(dp), dimension(6), intent(in)  :: param1
        real(dp), dimension(6)              :: x_out
        real(dp)                            :: a1, e1, i1, o1, o1c, M1, EA, theta

        a1      = param1(1)
        e1      = sqrt(param1(2)**2 + param1(3)**2)
        i1      = atan2(2*sqrt(param1(4)**2 + param1(5)**2), 1 - param1(4)**2 + param1(5)**2)
        o1c     = atan2(param1(5), param1(4))
        o1      = atan(param1(3)/param1(2)) - atan(param1(4)/param1(5))
        M1      = deg2rad(param1(6)) - (o1c + o1)
        EA      = EA_newton(M1, e1)
        theta   = 2.0*atan2(sqrt(1 - e1)*cos(EA/2.0), sqrt(1 + e1)*sin(EA/2.0))

        x_out = (/a1, e1, i1, o1, o1c, theta/)

    end subroutine

    function deg2rad(deg)
        integer, parameter :: dp=kind(0.d0)
        real(dp)           :: deg, deg2rad
        deg2rad = deg*pi/180.0
    end function

    function rad2deg(rad)
        integer, parameter  :: dp=kind(0.d0)
        real(dp)            :: rad, rad2deg
        rad2deg = rad*180.0/pi
    end function

end module
