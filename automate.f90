module automate
    use Neodys
    use Monte_Carlo
    use Int_sim
    use integrators
    use coord_trans_array
    use date_conversions
    use events_defined


    implicit none
    contains

    subroutine automate_dynamics()
        integer, parameter      :: dp=kind(0.d0)
        character(32)           :: name, folder_name
        integer                 :: i, j, start, end_time, time_main_start, time_main_end
        logical                 :: cond
        real(dp)                :: days

        time_main_start = time()
        open(Unit = 510, File = "ast_name.txt", form = 'Formatted', status = "OLD", Action = "READ")
        folder_name = 'automateOutputData'
        call file_open("outputWrite.dat", 520, folder_name)
        i = 0;     j=0;
        cond = .true.
        days = 36500.0_dp
        do while (j >= 0)
            READ(510,"(A12)",IOSTAT=j)name
            print *, 'Starting process for asteroid: ', name, i
            start = time()
            write (520,*) 'Starting process for asteroid: ', name, i
            call ss_output(name, days)
            end_time = time() - start
            write (520,*) 'Process for asteroid completed in ', end_time, 'seconds'
            i = i+1
            if (j < 0)then
                cond = .false.
                continue
            end if
        end do
        close(510)
        close(520)
        time_main_end = time() - time_main_start
        print*, 'Completed Dynamics Evaluation for all objects in ', time_main_end

    end subroutine


end module
