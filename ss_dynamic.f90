module ss_dynamic
    !*****************************************************************************************
    !*****************************************************************************************
    ! Module contains n-body integrator using 8 planets along with relativistic corrections  *
    !*****************************************************************************************
    !*****************************************************************************************
    !
    ! Written       :: Divas
    ! Created       :: Feb 2017
    ! Last updated  :: Aug 2017
    !
    !
    !
    ! Contains ss_int function to solve nbody problem at time t_in
    ! with initial conditions x_in
    !
    !Uses   ::constants to load gravitational parameters
    !         ephimeris to load planetary ephimeris from JPL ephimeris file
    !         coord_trans to change planetary state vector to ecliptic plane
    !
    !*****************************************************************************************
    !*****************************************************************************************
    use constants
    use ephimeris
    use coord_trans
    use file_chk

    implicit none
    contains

    function ss_int(x_in, t_in)
        integer, parameter                      :: dp = kind(0.d0)
        real(dp), dimension(6), intent(in)      :: x_in
        real(dp), intent(in)                    :: t_in
        integer                                 :: ncent, bodies, i, ii, j
        real(dp)                                :: cc, term1, term2, term3, term4, term5, term6, term7, relcorr,&
                                                    temp_norm, aa, bb, dd, ee
        real(dp), dimension(11)                 :: mu_bodies
        real(dp), dimension(6)                  :: rv_param, ss_int
        real(dp), dimension(3)                  :: rr, vv, r_obj, v_obj
        real(dp), allocatable                   :: speed(:), r_planet(:,:), v_planet(:,:), a_planet(:,:), temp(:),&
                                                   distance(:), distance_sqr(:), newton(:), temp2(:)

        aa = 1.0_dp;                    bb = 2.0_dp
        dd = 3.0_dp;                    ee = 4.0_dp
        cc = 29979.191025_dp            !speed of light in AU/day
        ncent = 12;                     bodies = 11
        allocate(speed(bodies+1));      allocate(r_planet(bodies, 3));   allocate(v_planet(bodies, 3))
        allocate(a_planet(bodies, 3));  allocate(distance(bodies));     allocate(distance_sqr(bodies))

!       loading planetary constants
        mu_bodies = gravp()

!       Planetary positions
        call PLEPH(t_in, 11, 12, rv_param)

        rr = eq2ecl(rv_param(1:3));                 vv = eq2ecl(rv_param(4:6))

        r_planet(1, 1:3) = rr;                      v_planet(1, 1:3) = vv
        speed(1) = norm2(vv)
        do i = 2, bodies
            call PLEPH(t_in, i-1, ncent, rv_param)
            rr= eq2ecl(rv_param(1:3));              vv = eq2ecl(rv_param(4:6))
            r_planet(i, :) = rr;                    v_planet(i, :) = vv
            speed(i) = norm2(vv)
        end do

!       Initial conditions
        r_obj = x_in(1:3);                       v_obj = x_in(4:6)
        ss_int = 0.0_dp

!        a_sun = sun_J2(x_in,t_in)

!       Acceleration of bodies
        do i = 1, bodies
            a_planet(i, 1:3) = 0.0_dp
            do j = 1, bodies
                if (j .ne. i) then
                    temp = r_planet(i, :) - r_planet(j, :)
                    temp = mu_bodies(j)*temp/norm2(temp)**3
                    a_planet(i,:) = a_planet(i, :) - temp
                end if
            end do
        end do

        do i = 1, bodies
                temp            = r_planet(i,:) - r_obj
                distance(i)     = dsqrt(temp(1)**2 + temp(2)**2 + temp(3)**2)
                distance_sqr(i) = temp(1)**2 + temp(2)**2 + temp(3)**2
        end do

        do i = 1, bodies
            newton = (r_planet(i, :) - r_obj)*mu_bodies(i)
            newton = newton/(distance(i)**3)

            term1  = 0.0_dp;      term2 = 0.0_dp
            term1 =  sum(mu_bodies/distance)
            term1 = term1*(-4.0_dp)

            do ii = 1, bodies
                if (ii.ne.i) then
                    temp = r_planet(ii, :) - r_planet(i, :)
                    term2 = term2 + mu_bodies(ii)/norm2(temp)
                end if
            end do
            term2 = (-1.0_dp)*term2
            temp  = v_planet(i, :)
            term3 = bb*(norm2(temp)**2)
            term4 = norm2(v_obj)**2
            term5 = (-ee)*dot_product(v_obj, temp)

            temp2 = r_obj - r_planet(i,:)
            term6 = (-dd/bb)*(dot_product(temp2,temp))**2/distance_sqr(i)

            temp  = a_planet(i,:)
            term7 = (-aa/bb)*dot_product(temp2,temp)

            relcorr = aa+(aa/cc)*(term1+term2+term3+term4+term5+term6+term7)

            temp  = r_obj - r_planet(i,:)
            temp2 = ee*v_obj - dd*v_planet(i,:)
            temp_norm = dot_product(temp2,temp)*mu_bodies(i)/distance(i)**3

            temp  = v_obj - v_planet(i,:)
            temp2 = (7.0_dp/bb)*a_planet(i,:)*mu_bodies(i)/distance(i)

            newton      = newton*relcorr + (aa/cc)*(temp*temp_norm+temp2)
            ss_int(4:6) = ss_int(4:6) + newton
        end do
        ss_int(1:3) = x_in(4:6)

    end function

!    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    subroutine dist_planet(r_in, t_in, body, dist)
        integer, parameter                      :: dp = kind(0.d0)
        real(dp), dimension(:,:), intent(in)    :: r_in
        real(dp), dimension(:),intent(in)       :: t_in
        integer, intent(in)                     :: body
        real(dp), allocatable                   :: dist(:)
        real(dp), dimension(6)                  :: param_out
        real(dp), dimension(3)                  :: temp, x_earth
        integer                                 :: i, n

        n = size(r_in, 1)
        allocate(dist(n))

        do i = 1, n
            call PLEPH(t_in(i), body, 12, param_out)
            x_earth = eq2ecl(param_out(1:3))
            temp    = r_in(i,1:3) - x_earth
            dist(i) = norm2(temp)
        end do


    end subroutine




end module
