module constants
    implicit none
    integer, parameter      :: d_p=kind(0.d0), ld = (SELECTED_REAL_KIND(15))
    real(ld), parameter     :: pi = 4 * atan (1.0_ld)
    real(d_p)               :: day = 86400.0_d_p, AU  = 149597870.69_d_p,&
                                inc =  (23.0_d_p+(26.0_d_p/60.0_d_p)+(21.448_d_p/3600.0_d_p))*pi/180.0_d_p,&
                                mu = 0.2959122082856201E-003_d_p

contains

    function gravp(arg)
        real(kind = 8), allocatable, dimension(:) :: gravp
        integer, optional                         :: arg
        real(ld), dimension(11)             :: temp

        temp = (/132712440018.0_ld, 22032.0_ld, 324859.0_ld, 398600.4328889457_ld, 42823.425807143_ld, &
                  126712767.8578078_ld, 37940626.06113729_ld, 5794549.007072442_ld, &
                  6836534.063972008_ld, 983.0551112444444_ld, 4902.800582049443_ld /)

        if(present(arg)) then
            allocate(gravp(1))
            gravp = temp(arg)*(day**2)/(AU**3)
        else
            gravp = temp*(day**2)/(AU**3)
        end if

    end function

end module
