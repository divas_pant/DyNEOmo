module Monte_Carlo
    !*****************************************************************************************
    !*****************************************************************************************
    ! Module to generate data points using MC simulation
    !*****************************************************************************************
    !*****************************************************************************************
    !
    ! Written       :: Divas
    ! Created       :: Feb 2017
    ! Last updated  :: Mar 2017
    !
    ! Modules       :: multivariate_normal
    !                  chol
    !                  random_normal - generating uniformly distributed independent variables
    !
    !*****************************************************************************************
    !*****************************************************************************************
    implicit none
    contains

    subroutine multivariate_rand(N, mean, cov, rand_out)
        !*****************************************************************************************
        !*****************************************************************************************
        ! Subroutine generates uniformly distributed random points for MC simulation using L mat *
        !*****************************************************************************************
        !*****************************************************************************************
        !
        ! Written       :: Divas
        ! Created       :: Feb 2017
        ! Last updated  :: Mar 2017
        !
        ! Input         :: n - number of data points
        !                  mean - mean vector containing initial condition for asteroid
        !                  cov- 6x6 covariance matrix of observation data
        !
        !*****************************************************************************************
        !*****************************************************************************************
        implicit none
        integer, parameter                      :: dp = kind(0.d0)
        integer, intent(in)                     :: N
        real(dp), intent(in), dimension(6)      :: mean
        real(dp), intent(in), dimension(6,6)    :: cov
        real(dp), allocatable, dimension(:,:)   :: rand_out
        integer                                 :: i, j
        real(dp), dimension(6,6)                :: chol_out
        real(dp), dimension(6)                  :: y

        if (n == 0 .or. n == 1) then
            allocate(rand_out(1,6))
            rand_out(1,:) = mean(:)
            return
        end if
        allocate(rand_out(n,6))
        rand_out(1,:) = mean(:)
        chol_out    = transpose(chol(cov, 6))
        do j = 2, N
            do i = 1, 6
                y(i) = random_normal()
            end do
            rand_out(j,1:6) = mean + matmul(chol_out, y)
        end do

    end subroutine

    function chol(mat_in, n)
        !*****************************************************************************************
        !*****************************************************************************************
        ! Subroutine to factorize PD matrix into it's lower triangular form **********************
        !*****************************************************************************************
        !*****************************************************************************************
        !
        ! Written       :: Divas
        ! Created       :: Feb 2017
        ! Last updated  :: Mar 2017
        !
        ! Input         :: N - dimension of matrix
        !                  mat_in - NxN positive definite matrix
        !
        !*****************************************************************************************
        !*****************************************************************************************
        integer, parameter                          :: dp = kind(0.d0)
        integer, intent(in)                         :: n
        real(dp), dimension(n, n), intent(in)       :: mat_in
        real(dp), dimension(n, n)                   :: chol
        real(dp), dimension(n, n)                   :: a
        integer                                     :: i, j, k
        real(dp)                                    :: s

        a = mat_in

        do k = 1, n
            do i = 1, k-1
                s = 0.0_dp
                do j = 1, i-1
                    s = s + (a(i, j)* a(k, j))
                end do
                a(k, i) = (a(k, i) - s)/a(i,i)
            end do

            do i = k, n-1
                a(k, i+1) = 0
            end do

            s = 0.0_dp
            do j = 1, k-1
                s = s + a(k, j)**2
            end do
            a(k,k) = sqrt(a(k,k) - s)
        end do

        chol = a

    end function

    FUNCTION random_normal()

        ! Adapted from the following Fortran 77 code
        !      ALGORITHM 712, COLLECTED ALGORITHMS FROM ACM.
        !      THIS WORK PUBLISHED IN TRANSACTIONS ON MATHEMATICAL SOFTWARE,
        !      VOL. 18, NO. 4, DECEMBER, 1992, PP. 434-435.

        !  The function random_normal() returns a normally distributed pseudo-random
        !  number with zero mean and unit variance.

        !  The algorithm uses the ratio of uniforms method of A.J. Kinderman
        !  and J.F. Monahan augmented with quadratic bounding curves.

        IMPLICIT NONE
        integer, parameter                          :: dp = kind(0.d0)

        !     Local variables
        REAL(dp)    :: s = 0.449871, t = -0.386595, a = 0.19600, b = 0.25472, &
                       r1 = 0.27597, r2 = 0.27846, u, v, x, y, q
        real(dp)    :: random_normal

        !     Generate P = (u,v) uniform in rectangle enclosing acceptance region


        DO
            CALL RANDOM_NUMBER(u)
            CALL RANDOM_NUMBER(v)
            v = 1.7156 * (v - 0.5)

            !     Evaluate the quadratic form
            x = u - s
            y = ABS(v) - t
            q = x**2 + y*(a*y - b*x)

            !     Accept P if inside inner ellipse
            IF (q < r1) EXIT
                !     Reject P if outside outer ellipse
                IF (q > r2) CYCLE
                !     Reject P if outside acceptance region
                    IF (v**2 < -4.0*LOG(u)*u**2) EXIT
        END DO

        !     Return ratio of P's coordinates as the normal deviate
        random_normal = v/u


    END FUNCTION random_normal


end module
