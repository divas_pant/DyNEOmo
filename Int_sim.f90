module Int_sim
    use Neodys
    use Monte_Carlo
    use ss_dynamic
    use integrators
    use coord_trans_array
    use date_conversions
    use events_defined

    implicit none
    contains

    subroutine MC_int(ast_name, n, days)
        integer, parameter              :: dp = kind(0.d0)
        character(*), intent(in)        :: ast_name
        integer, intent(in)             :: n
        real(dp)                        :: t0, days, tf
        real(dp), dimension(6)          :: x_in, x_t
        real(dp), allocatable           :: rv_ini(:,:), x_all(:,:), t_all(:), x_final(:,:), kep_ini(:,:),&
                                            dist(:), t_final(:), x_rand(:,:), kep_fin(:,:)
        real(dp), dimension(6,6)        :: cov_out
        integer                         :: i
        character(50)                       :: folder_name

        allocate(x_final(n, 6));    allocate(t_final(n))

        folder_name = 'MCSim_out\'//ast_name
        call file_open("initial_state_vecs.dat", 210, folder_name)
        call file_open("Initial_orbital_params.dat", 220, folder_name)
        call file_open("Final_state_vec.dat", 230, folder_name)
        call file_open("Final_dist_all.dat", 240, folder_name)
        call file_open("Final_orbital_param.dat", 250, folder_name)

        call load_asteroid(ast_name, t0, x_in, cov_out)
        x_in(6) = deg2rad(x_in(6))
        t0 = mjd2jd(t0)
        tf = t0+days

        call multivariate_rand(n, x_in, cov_out, x_rand)

        call EQ2BCE(x_rand, t0, rv_ini)
        call statevec2kep(rv_ini, kep_ini)


        do i = 1, n
            call RKFcall(ss_int, rv_ini(i,:), t0, tf, x_t, tf, x_all, t_all)
            x_final(i,:) = x_t
            t_final(i)   = tf
        end do
        call dist_planet(x_final, t_final, 3, dist)

        call statevec2kep(x_final, kep_fin)

        do i = 1, n
            write(210, *)rv_ini(i,:)
            write(220, *)kep_ini(i,:)
            write(230, *)x_final(i,:)
            write(240, *)dist(i)
            write(250, *)kep_fin(i,:)
        end do

        close(201);  close(220); close(230); close(240)
        close(250)

    end subroutine

    subroutine ss_output(ast_name, days)
        integer, parameter                  :: dp = kind(0.d0)
        character(*), intent(in)            :: ast_name
        real(dp), intent(in)                :: days
        real(dp)                            :: t1, t0, t_fin
        real(dp), dimension(6)              :: x_in, ini_kep, x_out, fin_kep
        real(dp), dimension(6,6)            :: cov_out
        real(dp), allocatable               :: x_all(:,:), x_orbital(:,:), t_all(:), dist(:), x_events(:,:), t_events(:)
        integer                             :: i, n, k
        character(50)                       :: folder_name

        folder_name = 'Dynamics_out\'//ast_name
        call file_open("state_vec.dat", 210, folder_name)
        call file_open("orbital_param.dat", 220, folder_name)
        call file_open("dist_earth.dat", 230, folder_name)
        call file_open("output.dat", 240, folder_name)
        call file_open("events.dat", 250, folder_name)

        call load_asteroid(ast_name, t0, x_in, cov_out, .true.)
        if (int(t0) == 0) then
            return
        end if
        x_in(6) = deg2rad(x_in(6))
        t0 = mjd2jd(t0)
        t_fin = t0+days

         call MEE2BCE(x_in, t0, x_in)

        call RKFcall(ss_int, x_in, t0, t_fin, x_out, t1, x_all, t_all, earth_closest_encounter, x_events, t_events)

        n = size(x_all, 1)
        k = size(x_events, 1)

        call statevec2kep(x_all, x_orbital)
        call dist_planet(x_all, t_all, 3, dist)
        call rv2orbparam(x_in(1:3), x_in(1:3), ini_kep)
        call rv2orbparam(x_out(1:3), x_out(4:6), fin_kep)

        write(240, 100)'Initial State vectors:    ',x_in
        write(240, 100)'Initial Orbital Elements: ', ini_kep
        write(240, 100)'Final Orbital Elements:   ', fin_kep
        write(240, 101)'Final Distance:', dist(n)
        write(240, 101)'Initial Time:  ', t0
        write(240, 101)'Final Time:    ', t_fin


        do i = 1, n
            write(210,*) x_all(i,:)
            write(220,*) x_orbital(i,:)
            write(230,*) dist(i), '', t_all(i)
        end do

        do i = 1, k
            write(250, *) x_events(i,:), '', t_events(i)
        end do

        100 format(' ', A26,/, 1F35.17,/,1F35.17,/,1F35.17,/,1F35.17,/,1F35.17,/,1F35.17,/)
        101 format(' ', A15,/, 1F35.17,/)

        close(210); close(220); close(230); close(240); close(240)

    end subroutine


    SUBROUTINE init_random_seed()
        INTEGER                             :: i, n, clock
        INTEGER, DIMENSION(:), ALLOCATABLE  :: seed

        CALL RANDOM_SEED(size = n)
        ALLOCATE(seed(n))

        CALL SYSTEM_CLOCK(COUNT=clock)

        seed = clock + 37 * (/ (i - 1, i = 1, n) /)
        CALL RANDOM_SEED(PUT = seed)

        DEALLOCATE(seed)
    END SUBROUTINE

end module
