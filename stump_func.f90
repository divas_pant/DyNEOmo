module stump_func
    !*****************************************************************************************
    !*****************************************************************************************
    ! needed to solve keplers equation *******************************************************
    ! based on Howard curtis algorithm *******************************************************
    !*****************************************************************************************
    !
    ! Written       :: Divas
    ! Created       :: Jan 2017
    ! Last updated  :: Jan 2017
    !
    !*****************************************************************************************
    !*****************************************************************************************
    implicit none
    contains

    real(kind = 8) function stumpc(z)
        integer, parameter :: dp=kind(0.d0)
        real(dp) :: z, c

        if (z > 0.0_dp) then
            c = (1 - cos(sqrt(z)))/z;
        elseif (z < 0.0_dp)  then
            c = (cosh(sqrt(-z)) - 1)/(-z);
        else
            c = 1.0/2.0
        end if
        stumpc = c
    end function

    real(kind = 8) function stumps(z)
        integer, parameter :: dp=kind(0.d0)
        real(dp) :: s, z

        if (z > 0.0) then
            s = (sqrt(z) - sin(sqrt(z)))/(sqrt(z))**3
        elseif (z<0.0) then
            s = (sinh(sqrt(-z)) - sqrt(-z))/(sqrt(-z))**3
        else
            s = 1.0/6.0
        end if
        stumps = s
    end function

end module
