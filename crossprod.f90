module crossprod
    implicit none
    contains

    function cross_product(Vec_a, Vec_b)

        integer, parameter :: dp=kind(0.d0)
        real(dp), dimension(3) :: cross_product
        real(dp), intent(in), dimension(3) :: vec_a, vec_b


        cross_product(1) = vec_a(2)*vec_b(3)-vec_a(3)*vec_b(2)
        cross_product(2) = vec_a(3)*vec_b(1)-vec_a(1)*vec_b(3)
        cross_product(3) = vec_a(1)*vec_b(2)-vec_a(2)*vec_b(1)

    end function
end module
