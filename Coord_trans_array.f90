! contains coordinate transformations from kepler to state vector and vice versa
module coord_trans_array
    use constants
    use crossprod
    use ephimeris
    use kepler_solver
    use coord_trans

    implicit none

    contains

    subroutine statevec2kep(param_in, param_out)
    !***************************************************************************************************
    !***************************************************************************************************
    !*** Converts cartesian coordinate in ECEF into latitude, longitude and elevation
    !*** Last Modified : Nov-2017
    !*** All units in AU
    !*** param _in = set of pos and vel vector(r1, r2, r3, v1, v2, v3)
    !*** param_out = set of keplers parameter(sma, ecc, omega, Omega, theta)
    !***************************************************************************************************
    !***************************************************************************************************
        integer, parameter                                  :: dp = kind(0.d0)
        real(dp), parameter, dimension(3)                   :: I_vec = (/0.0_dp, 0.0_dp, 1.0_dp/)
        real(dp), dimension(:,:), intent(in)                :: param_in
        real(dp), allocatable                               :: param_out(:,:)
        real(dp)                                            :: eps, r_norm, v_norm, v_radial, H_norm,&
                                                                Ecc_norm, inclination, omega_norm, &
                                                                omegac_norm, n_norm, T_A, sma, mu_in
        real(dp), dimension(3)                              :: H_vec, N_vec, ecc_vec, cp, r_in, v_in
        integer                                             :: i, n

        n = size(param_in, 1)
        allocate(param_out(n,6))

        eps = 1e-12_dp;                     mu_in = 0.2959122082856201E-003

        do i = 1, n
            r_in = param_in(i,1:3);        v_in = param_in(i, 4:6)

            r_norm   = norm2(r_in);         v_norm = norm2(v_in)
            v_radial = dot_product(r_in, v_in)/r_norm

            h_vec  = cross_product(r_in, v_in)
            h_norm = norm2(h_vec)

            inclination = dacos(H_vec(3)/H_norm)

            n_vec  = cross_product(I_vec, H_vec)
            n_norm = norm2(N_vec)

            if (n_norm /= 0.0) then
                omegac_norm = dacos(n_vec(1)/n_norm)
                if (N_vec(2) < 0.0) then
                    omegac_norm = 2*pi-omegac_norm
                end if
            else
                omegac_norm = 0
            end if

            ecc_vec  = 1/mu_in*((v_norm**2-mu_in/r_norm)*r_in - r_norm*v_radial*v_in)
            ecc_norm = norm2(ecc_vec)

            if (n_norm /= 0.0) then
                if (Ecc_norm > eps) then
                    omega_norm = dacos(dot_product(N_vec, ecc_vec)/n_norm/Ecc_norm)
                    if (ecc_vec(3) < 0.0) then
                        omega_norm = 2*pi - omegac_norm
                    end if
                else
                    omega_norm = 0.0_dp
                end if
            else
                omega_norm = 0.0_dp
            end if

            if (Ecc_norm > eps) then
                T_A = dacos(dot_product(ecc_vec, r_in)/ecc_norm/r_norm)
                if (v_radial < 0.0) then
                    T_A = 2*pi-T_A
                end if
            else
                cp = cross_product(N_vec, r_in)
                if (cp(3) >= 0) then
                    T_A = acos(dot_product(N_vec, r_in)/n_norm/r_norm)
                else
                    T_A = 2*pi - acos(dot_product(N_vec, r_in)/n_norm/r_norm)
                end if
            end if

            sma = H_norm**2/mu_in/(1-Ecc_norm**2)

            param_out(i,:) = (/sma, Ecc_norm, inclination, omega_norm, omegac_norm, T_A/)

        end do

    end subroutine


    subroutine kep2statevec(param_in, param_out)
    !***************************************************************************************************
    !***************************************************************************************************
    !*** Converts orbital elements to position and velocity
    !*** Last Modified : Nov-2017
    !*** All units in AU and AU/Day
    !*** x _in = set of kepler parameter(a, e, i, omega, Omega, theta)
    !*** x_out = set of pos. and vel. vectors(r1, r2, r3, v1, v2, v3)
    !***************************************************************************************************
    !***************************************************************************************************
        integer, parameter                      :: dp = kind(0.d0)
        real(dp), intent(in), dimension(:,:)    :: param_in
        real(dp)                                :: sma, ecc, inclination, omega, omegaC, theta, h, mu_in
        real(dp),DIMENSION(3)                   :: r_out, v_out
        real(dp),DIMENSION(3,3)                 :: R3_OmegaC, R1_i, R3_omega, Qpx
        real(dp), allocatable                   :: param_out(:,:)
        integer                                 :: n, i

        n = size(param_in, 1)
        allocate(param_out(n, 6))
        mu_in = 0.2959122082856201E-003

        do i = 1, n
            sma = param_in(i, 1);           ecc = param_in(i, 2);
            inclination = param_in(i, 3);   omega = param_in(i, 4);
            omegaC = param_in(i, 5);        theta = param_in(i, 6)

            h = sqrt(sma*mu_in*(1-ecc**2))

            r_out(1) = cos(theta)*((h**2)/mu_in)/(1 + ecc*cos(theta));
            r_out(2) = sin(theta)*((h**2)/mu)/(1 + ecc*cos(theta))
            r_out(3) = 0.0_dp

            v_out(1) = -(mu_in/h) *sin(theta);
            v_out(2) = (mu_in/h) *(ecc+cos(theta));
            v_out(3) = 0.0_dp
            R3_OmegaC = 0.0_dp
            R1_i      = 0.0_dp

            R3_OmegaC(1,1) = dcos(OmegaC);       R3_OmegaC(1,2) = dsin(OmegaC);
            R3_OmegaC(2,1) = -dsin(Omegac);      R3_OmegaC(2,2) = dcos(OmegaC);
            R3_OmegaC(3,3) = 1.0_dp

            R1_i(1,1) = 1.0_dp;
            R1_i(2,2) = dcos(inclination);          R1_i(2,3) = dsin(inclination)
            R1_i(3,2) = -dsin(inclination);         R1_i(3,3) = dcos(inclination)

            R3_omega(1,1) = dcos(omega);         R3_omega(1,2) = dsin(omega);
            R3_omega(2,1) = -dsin(omega);        R3_omega(2,2) = dcos(omega);
            R3_omega(3,3) = 1.0_dp

            qpx = matmul(transpose(R3_OmegaC),transpose(R1_i))
            qpx = matmul(qpx,transpose(R3_omega))

    !        x_out(1:3) = matmul(qpx, r_out)
    !        x_out(4:6) = matmul(qpx, v_out)
            param_out(i, 1:3) = r_out
            param_out(i, 4:6) = v_out
        end do

    end subroutine

    subroutine Eq2BCE(x_in, t_in, x_out)
    !***************************************************************************************************
    !***************************************************************************************************
    !*** Converts Equinoctial elements into cartesian coordinates
    !*** Last Modified : Mar-2017
    !*** All units in AU and AU/day
    !*** x _in = set of Equinoctial elements(a, p1, p2, q1, q2, l)
    !*** x_out = set of pos. and vel. vectors(r1, r2, r3, v1, v2, v3)
    !***************************************************************************************************
    !***************************************************************************************************
        integer, parameter                      :: dp = kind(0.d0)
        real(dp)                                :: sma, p1, p2, q1, q2, ml, k, f, fp, e2, rm, ab, sinl, &
                                                    cosl, dum, h, p,  order, mu, t_in
        integer                                 :: i, j, ii, n
        real(dp), dimension(:,:), intent(in)    :: x_in
        real(dp), allocatable                   :: x_out(:,:)
        real(dp), dimension(6)                  :: rv_param
        real(dp), dimension(3,3)                :: AA
        real(dp), dimension(3)                  :: r, v, rr, vv, r_sun, v_sun

        n = size(x_in, 1)
        allocate(x_out(n, 6))

        order = 2;                          mu = 0.2959122082856201d-003
!       Sun position in Solar system barycenter equatorial J2000
        call PLEPH(t_in, 11, 12, rv_param)

!       Sun position in Solar system barycenter ecliptic
        r_sun(1:3) = eq2ecl(rv_param(1:3))
        v_sun(1:3) = eq2ecl(rv_param(4:6))

        do ii = 1, n

            sma = x_in(ii,1);                      p1 = x_in(ii,2)
            p2  = x_in(ii,3);                      q1 = x_in(ii,4)
            q2  = x_in(ii,5);                      ml = x_in(ii,6)

            k = ml
            f = k+p1*dCOS(K)-p2*dSIN(K)-ml
            do WHILE (ABS(f)>1e-15)
               f  = k+p1*dCOS(k)-p2*dSIN(k)-ml
               fp = 1-P1*dSIN(K)-P2*dCOS(K)
               k  = k-f/fp
            end do

            f = k+p1*dCOS(k)-p2*dSIN(k)-ml

            do i = 1, INT(LOG(order+1)/LOG(2.0))+1;
               f  = k+p1*dCOS(k)-p2*dSIN(k)-ml;
               fp = 1-p1*dSIN(k)-p2*dCOS(k)
               k  = k-f/fp;
            end do

            e2   = P1**2 + P2**2
            p    = sma*(1-e2)
            h    = dSQRT(p * mu)
            rm   = sma*(1-(p1*dSIN(k))-(p2*dCOS(k)))
            ab   = 1/(1+dsqrt(1-P1**2-P2**2))
            sinl = sma/rm*((1-ab*p2**2)*dSIN(k) + ab*p1*p2*dCOS(k)-p1)
            cosl = sma/rm*((1-ab*p1**2)*dCOS(k) + ab*p1*p2*dSIN(k)-p2)


            r(1) = rm*cosl
            r(2) = rm*sinl
            r(3) = 0.0_dp
            v(1) = h/p*(-p1-sinl)
            v(2) = h/p*(p2+cosl)
            v(3) = 0.0_dp

    !       r and v in inertial Sun centered ecliptic
            dum     = 1/(1+Q1**2+Q2**2);
            AA(1,1) = dum*(1-Q1**2+Q2**2);       AA(1,2) = dum*2*Q1*Q2;
            AA(1,3) = dum*2*Q1;                  AA(2,1) = dum*2*Q1*Q2;
            AA(2,2) = dum*(1+Q1**2-Q2**2);       AA(2,3) = dum*(-2*Q2**2);
            AA(3,1) = dum*(-2*Q1);               AA(3,2) = dum*2*Q2;
            AA(3,3) = dum*(1-Q1**2-Q2**2)

            do i = 1, 3
                rr(i) = 0.0_dp
                vv(i) = 0.0_dp
                do j = 1, 3
                    rr(i) = rr(i)+aa(i,j)*r(j)
                    vv(i) = vv(i)+aa(i,j)*v(j)
                end do
            end do

    !       Asteroid position in Solar system barycenter ecliptic
           x_out(ii, 1:3)   = r_sun + rr
           x_out(ii, 4:6)   = v_sun + vv
        end do


    end subroutine

    subroutine Eq2Kep(param_in, param_out) ! Omega and theta not correct
        integer, parameter                                  :: dp = kind(0.d0)
        real(dp), dimension(:,:), intent(in)                :: param_in
        real(dp), dimension(:,:)                            :: param_out
        real(dp)                                            :: a1, e1, i1, o1, o1c, theta
        integer                                             :: i, n

        n = size(param_in,1)
!        allocate(param_out(n, 6))

        do i = 1, n
            a1      = param_in(i, 1)
            e1      = dsqrt(param_in(i, 2)**2 + param_in(i, 3)**2)
            i1      = datan2(2*dsqrt(param_in(i, 4)**2 + param_in(i, 5)**2), 1 - param_in(i, 4)**2 - param_in(i, 5)**2)
            o1c     = datan2(param_in(i, 4), param_in(i, 5))
            o1      = datan2(param_in(i, 2)*param_in(i, 5)- param_in(i, 3)*param_in(i, 4), param_in(i, 2)*&
                        param_in(i, 4)+param_in(i, 3)*param_in(i, 5))
            theta   = param_in(i, 6)-(o1+o1c)

            param_out(i,:)   = (/a1, e1, i1, o1, o1c, theta/)

        end do

    end subroutine

    subroutine Kep2Eq(x_in, x_out)
    !***************************************************************************************************
    !***************************************************************************************************
    !*** Converts Keplerian parameters into Equinoctial elements
    !*** Last Modified : Mar-2017
    !*** x _in = set of kepler parameter(a, e, i, omega, Omega, theta)
    !*** x_out = set of equinoctial elements(a, p1, p2, q1, q2, l)
    !***************************************************************************************************
    !***************************************************************************************************
        integer, parameter                      :: dp = kind(0.d0)
        real(dp), dimension(:,:), intent(in)    :: x_in
        real(dp), dimension(:,:)                :: x_out
        real(dp)                                :: sma, p1, p2, q1, q2, l, EA, MA, esqrt
        integer                                 :: n, i

        n = size(x_in, 1)

        do i = 1, n
            esqrt = 1.0/dsqrt((1+x_in(i, 2))/(1-x_in(i, 2)))
            EA = 2*datan(esqrt*dtan(x_in(i, 6)/2.0))
            MA = EA - x_in(i, 2)*dsin(EA)

            sma = x_in(i, 1)
            p1 = x_in(i, 2)*dsin(x_in(i, 4)+x_in(i, 5));
            p2 = x_in(i, 2)*dcos(x_in(i, 4)+x_in(i, 5))
            q1 = dtan(x_in(i, 3)/2)*dsin(x_in(i, 5));
            q2 = dtan(x_in(i, 3)/2)*dcos(x_in(i, 5))
            l = MA+ x_in(i, 4)+x_in(i, 5)
            x_out(i, :) = (/sma, p1, p2, q1, q2, l/)

        end do

    end subroutine


end module
