module kepler_solver
    use stump_func
    use constants
    implicit none
    contains


    real(kind = 8) function EA_newton(M, ecc)

    integer, parameter  :: dp=kind(0.d0)
    real(dp)            :: e_rror = 1e-10, ratio = 1, F, E
    real(dp),intent(IN) ::M, ecc

    if (ecc < 1.0) then
        if (M < pi) then
            E = M+ecc/2.0
        else
            E = M-ecc/2.0
        end if

        ratio = 1
        do while (abs(ratio) > e_rror)
            ratio = (E-ecc*sin(E)-M)/(1-ecc*cos(E))
            E = E - ratio
        end do
        EA_newton = E
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    elseif (ecc > 1.0) then

        F = M
        ratio = 1
        do while(abs(ratio) > e_rror)
            ratio = (ecc*sinh(F) - F - M)/(ecc*cosh(F)-1)
            F = F - ratio
        end do
        EA_newton = F
    else
        EA_newton = 0.0
    end if

    end function


    real(kind = 8) function kepler_u(dt, r_0, vr_0, a)
        integer, parameter :: dp=kind(0.d0)
        integer(kind = 8) :: n_max = 1000, n = 0
        real(dp) :: e_rror = 1e-10, x, C, S, f, dfdx, ratio = 1
        real(dp), Intent(IN) :: dt, r_0, vr_0, a

        x = sqrt(mu)*abs(a)*dt
        do while (abs(ratio) > e_rror .and. n <= n_max)
            n = n+1
            C = stumpc(a*x**2)
            S = stumps(a*x**2)
            F = r_0*vr_0/sqrt(mu)*x**2*C + (1 - a*r_0)*x**3*S + r_0*x-sqrt(mu)*dt
            dFdx = r_0*vr_0/sqrt(mu)*x*(1 - a*x**2*S)+(1 - a*r_0)*x**2*C+r_0
            ratio = F/dfdx
            x = x-ratio
        end do
       ! print *, 'Maximum Iterations', n
        kepler_u = x


    end function

end module
