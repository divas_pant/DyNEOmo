module events_defined
    use ephimeris
    use coord_trans

    implicit none
    contains
    function earth_closest_encounter(X, T)
        integer, parameter                  :: dp=kind(0.d0)
        real(dp), intent(in), dimension(6)  :: X
        real(dp), intent(in)                :: T
        real(dp)                            :: earth_closest_encounter
        real(dp), dimension(3)              :: x_earth, v_earth, x_relative, v_relative
        real(dp), dimension(6)              :: param_earth

        call PLEPH(T, 3, 12, param_earth)
        x_earth    = eq2ecl(param_earth(1:3));      v_earth = eq2ecl(param_earth(4:6))
        x_relative = X(1:3) - x_earth;              v_relative = X(4:6) - v_earth

        earth_closest_encounter = dot_product(x_relative, v_relative)

    end function


    function event_SOI(X, T)
            integer, parameter                      :: dp = kind(0.d0)
            real(dp), dimension(6), intent(in)      :: X
            real(dp), intent(in)                    :: T
            real(dp)                                :: event_SOI, dist, dist_SOI
            real(dp), dimension(6)                  :: param_out, temp

            dist_SOI = 791000.0_dp/AU
            call PLEPH(T, 3, 12, param_out)
            param_out(1:3)  = eq2ecl(param_out(1:3));       param_out(4:6) = eq2ecl(param_out(4:6))
            temp            = X - param_out
            dist            = norm2(temp(1:3))

            event_SOI = dist - dist_SOI

    end function
end module
