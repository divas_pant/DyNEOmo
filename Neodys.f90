module Neodys
    implicit none
    contains

    subroutine load_asteroid(name, time, param, cov, termination)
    !*****************************************************************************************
    !*****************************************************************************************
    ! Subroutine loads the equinoctial elements of the selected asteroid from NEODYS database*
    !*****************************************************************************************
    !*****************************************************************************************
    !
    ! Written       :: Divas
    ! Created       :: Feb 2017
    ! Last updated  :: Feb 2017
    !
    !
    !
    ! Input: name  (String containing the designated name of NEO)
    ! Output: Time (epoch of last updated database in MJD)
    !         param(6 element array containing equinoctial elements of the selected body for given epoch)
    !               (a in AU, l in degree)
    !         RMS   (6 element array containing residuals of each element{root of diagonals of covariance})
    !         cov  (6x6 covariance matrix corresponding to the observations)
    !         RMS or covariance can be change in the output at function call
    !         cov_out (21 array element of covarince elements
    !
    !*****************************************************************************************
    !*****************************************************************************************

        integer, parameter                  :: dp=kind(0.d0)
        character(len = *), intent(in)      :: name
        logical, intent(in), optional       :: termination
        character(len = 16)                 :: header
        real(dp)                            :: time, param(6), cov(6,6), RMS(6), cov_out(21)
        integer                             :: i, err_flag, j
        real(dp)                            :: sigma_a, sigma_p1, sigma_p2, sigma_q1, sigma_q2, sigma_l, &
                                               a, p1, p2, q1, q2, l, c11, c12, c13, c14, c15, c16, c22, &
                                               c23, c24, c25, c26, c33, c34, c35, c36, c44, c45, c46, &
                                               c55, c56, c66

        open(Unit = 10, File = "neodys.ctc", form = 'Formatted', status = "OLD", Action = "READ")

        i = 0;              j = 0
        do while(j >= 0)
            READ(10,*,IOSTAT=j)header
            if (header == name)goto 30
            if (j < 0)then
                print *, "Selected Asteroid not in the database"
                if (present(termination))then
                    time = 0.0; param = 0.0; cov = 0.0
                    return
                else
                    print *, "Exiting Program..."
                    print *, "  "
                    write( *, * ) 'Press Enter to continue'
                    read( *, * )
                    err_flag = 1
                    STOP
                end if
            end if
        end do


20      REWIND(10)

30      backspace(10)
        i = 0; j = 0
        do j = 1, 11
            READ(10,*, END = 20) header
            if (header == 'EQU') then
                backspace(10)
                read(10, *)header, a, p1, p2, q1, q2, l

            elseif (header == 'MJD') then
                backspace(10)
                read(10, *) header, time

            elseif (header == '!') then
                i = i+1
                if(i == 3) then
                    backspace(10)
                    read(10, *)header, header, sigma_a, sigma_p1, sigma_p2, sigma_q1, sigma_q2, sigma_l
                end if
!
            elseif (header == 'COV') then
                backspace(10)
                read(10, *)header, c11, c12, c13
                read(10, *)header, c14, c15, c16
                read(10, *)header, c22, c23, c24
                read(10, *)header, c25, c26, c33
                read(10, *)header, c34, c35, c36
                read(10, *)header, c44, c45, c46
                read(10, *)header, c55, c56, c66
            end if
        end do
        close(10)
        param = (/a, p1, p2, q1, q2, l/)
        RMS = (/sigma_a, sigma_p1, sigma_p2, sigma_q1, sigma_q2, sigma_l/)
        cov_out = (/c11, c12, c13, c14, c15, c16, c22, &
                    c23, c24, c25, c26, c33, c34, c35, &
                    c36, c44, c45, c46, c55, c56, c66/)

        cov(1,1) = c11;     cov(1,2) = c12;     cov(1,3) = c13;     cov(1,4) = c14; &
        cov(1,5) = c15;     cov(1,6) = c16;     cov(2,1) = c12;     cov(2,2) = c22; &
        cov(2,3) = c23;     cov(2,4) = c24;     cov(2,5) = c25;     cov(2,6) = c26; &
        cov(3,1) = c13;     cov(3,2) = c23;     cov(3,3) = c33;     cov(3,4) = c34; &
        cov(3,5) = c35;     cov(3,6) = c36;     cov(4,1) = c14;     cov(4,2) = c24; &
        cov(4,3) = c34;     cov(4,4) = c44;     cov(4,5) = c45;     cov(4,6) = c46; &
        cov(5,1) = c15;     cov(5,2) = c25;     cov(5,3) = c35;     cov(5,4) = c45; &
        cov(5,5) = c55;     cov(5,6) = c56;     cov(6,1) = c16;     cov(6,2) = c26; &
        cov(6,3) = c36;     cov(6,4) = c46;     cov(6,5) = c56;     cov(6,6) = c66



    end subroutine
end module
