#Make Fortran DyNEOMo Compiler 
#################################
SRC := date_conversions.f90 Neodys.f90 constants.f90 crossprod.f90 random_sampling.f90 file_chk.f90 ephimeris.f90 stump_func.f90 kepler_solver.f90 coord_trans.f90 Coord_trans_array.f90 ss_dynamic.f90 integrators.f90 events_defined.f90 Int_sim.f90 automate.f90

OBJECT := ./modules
MODULE := ./mod
OBJDIR := modules

GF := gfortran
objects := ${SRC:.f90=.o}


dyneomo: $(objects) main.o
	$(GF) -o dyneomo $? -J$(OBJECT) -I$(OBJECT)
	
main.o: main.f95
	$(GF) -c main.f95 -J$(OBJECT) -I$(OBJECT)

%.o: %.f90
	$(GF) -c $< -J$(OBJECT) -I$(OBJECT)


$(objects): |$(OBJDIR)

$(OBJECT):
	mkdir $(OBJDIR)
	

clean:
	-rm $(objects) main.o dyneomo 
	rm -rf $(OBJECT)
