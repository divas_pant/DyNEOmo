module file_chk
    implicit none

    contains

    subroutine file_open(name, unit_in, folder)
        integer, parameter                  :: dp = kind(0.d0)
        character(*), intent(in)            :: name
        character(*), intent(in), optional  :: folder
        character(155)                      :: dir_path
        integer                             :: unit_in, ierr
        logical                             :: exists

        exists = .false.
        CALL getcwd(dir_path)

        if(present(folder))then
            inquire( file=trim(folder)//'/.', exist=exists )
            if(exists .eqv. .false.)then
                call execute_command_line('mkdir .\'//folder)
            end if
            dir_path = trim(dir_path)//'\'//folder
        end if

        inquire(file = trim(dir_path)//'\'//trim(name), exist = exists, IOSTAT=IERR)
        if (exists)then
            open(Unit = unit_in, File = trim(dir_path)//'\'//trim(name), form = 'Formatted', status = "Unknown", Action = "Write")
        else
            open(Unit = unit_in, File = trim(dir_path)//'\'//trim(name), form = 'Formatted', status = "New", Action = "Write")

        end if

    end subroutine


end module
