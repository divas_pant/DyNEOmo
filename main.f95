program main
    use Int_sim
    use automate

    implicit none

    integer, parameter                      :: dp = kind(0.d0)
    integer                                 :: i, N, choice
    real(dp)                                :: days
    character(15), save                     :: ast_name

    call init_random_seed()
    i = 0

    print *,'****************************************************'
    print *,'.-.-.-.-.-.-.-.-.- DyNEOmo -.-.-.-.-.-.-.-.-.-.-.-.|'
    print *,'|--------------------------------------------------|'
    print *,'|Dynamical Near Earth Object Modeling              |'
    print *,'|--------------------------------------------------|'
    print *,'|--------------------------------------------------|'
    print *,'|This program simulates the dynamics of Near       |'
    print *,'|Earth Asteroids and output the data in tab format |'
    print *,'|--------------------------------------------------|'
    print *,'****************************************************'
    print *,'   '


!
10  Print *, 'Select from the following options'
    print *, '1. Monte-Carlo Simulation'
    print *, '2. Dynamics Evaluation'
    print *, '3. Begin Automate Dynamics'
    read(*,*) choice
!
    select case(choice)
    case(1)
        Print*, 'Select Asteroid to be Evaluated (Use designated IAU names eg: 99942 )'
        read(*,*) ast_name
        ast_name = trim(ast_name)
        Write(6, *) 'Enter number of MC samples to be evaluated'
        read(*,*) N

        Write(6, *) 'Number of days the motion is integrated for'
        read(*,*) days

        call MC_int(ast_name, N, days)

        Write(6, *)'Output is available in the following files'
        Write(6, *)"initial_state_vecs.dat"
        Write(6, *)"Initial_orbital_params.dat"
        Write(6, *)"Final_state_vec.dat"
        Write(6, *)"Final_dist_all.dat"
        Write(6, *)"Final_orbital_param.dat"


    case(2)
        Print*, 'Select Asteroid to be Evaluated (Use designated IAU names eg: 99942 )'
        read(*,*) ast_name
        ast_name = trim(ast_name)

        Write(6, *) 'Enter number of days for integration'
        read(*,*) days

        call ss_output(ast_name, days)

        Write(6, *)'Output is available in the following files'
        Write(6, *)"events.dat"
        Write(6, *)"state_vec.dat"
        Write(6, *)"orbital_param.dat"
        Write(6, *)"dist_earth.dat"
        Write(6, *)"output.dat"

    case(3)
        Print *, 'Starting Automated process'
        call automate_dynamics()

    case default
        i = i+1
        if (i .gt. 3)then
            print*, 'You have exceeded maximum tries'
            print*, 'Exiting program...'
            write( *, * ) ' '
            write( *, * ) 'Press Enter to continue'
            read( *, * )
            Stop
        end if
        print *, 'Incorrect Selection, Try Again'
        go to 10
    end select

    write( *, * ) ' '
    write( *, * ) 'Press Enter to continue'
    read( *, * )

end
