module integrators
    use file_chk
    implicit none
    contains

    function maxab(a,b)
        integer, parameter          :: dp = kind(0.d0)
        real(dp), intent(in)        :: a, b
        real(dp)                    :: maxab
        maxab = merge(a, b, a .gt. b)
    end function

    function minab(a,b)
        integer, parameter          :: dp = kind(0.d0)
        real(dp), intent(in)        :: a, b
        real(dp)                    :: minab
        minab = merge(a, b, a .lt. b)
    end function


    recursive subroutine RKFcall(fx, x0, t0, tf, x_out, t_out, x_all, t_all, events, x_events, t_events, terminal)
    !*****************************************************************************************
    !Wrapper function to call RKF78 function
    !*****************************************************************************************
        Interface
            FUNCTION fx(x_0, t_0)
                integer, parameter          :: dp=kind(0.d0)
                REAL(dp), INTENT(IN)        :: x_0(6), t_0
                REAL(dp), dimension(6)      :: fx
            END FUNCTION fx
        END INTERFACE

        integer, parameter                      :: dp=kind(0.d0)
        real(dp), external, optional            :: events
        logical, optional                       :: terminal
        real(dp), intent(in), dimension(6)      :: x0
        real(dp), intent(in)                    :: t0
        real(dp), allocatable, optional         :: x_events(:,:), t_events(:)
        real(dp)                                :: t_out, h, hh0, hh1, err_max, err_min, tf, x_out(6)
        real(dp), allocatable                   :: x_all(:,:), t_all(:)
        logical                                 :: terminal_cond

        err_max = 1e-15_dp;                 err_min = 1e-16_dp
        h       = 0.1e-3_dp;                hh0     = 1e-5_dp;         hh1 = 0.50_dp
        if (present(terminal))then
            terminal_cond = terminal
        else
            terminal_cond = .false.
        end if
        if (present(events)) then
            call RKF78(fx, x0, hh0, h, hh1, err_min, err_max, t0, tf, x_out, t_out, x_all, t_all, events, x_events,&
                        t_events, terminal_cond)
        else
            call RKF78(fx, x0, hh0, h, hh1, err_min, err_max, t0, tf, x_out, t_out, x_all, t_all)
        end if

    end subroutine

    recursive subroutine RKF78(fx, x_0, hmin, hs, hmax, err_min, err_max, t_0, t_f, x_out, t_out,&
                                x_all, t_all, events, x_out_events, t_out_events, terminal)
    !*****************************************************************************************
    !*****************************************************************************************
    ! Subroutine Runge-Kutta-Fehlberg Predictor Corrector                                    *
    !*****************************************************************************************
    !*****************************************************************************************
    !
    ! Written       :: Divas
    ! Created       :: Jan 2017
    ! Last updated  :: Nov 2017
    !
    !
    !
    ! Input:    fx  (function to be integrated, change dimension in interface if /= 6)
    !           x_0 (initial conditions for ODE)
    !           hmin, hs, hmax (minimum, default, maximum stepsizes)
    !           err_min, err_max (minimum and maximum error)
    !           t_0, t_f (initial and final value of free parameter)
    ! Output:   x_out (6 element array containing solution of f_x at t = t_t )
    !           t_out (final time)
    !
    !
    !*****************************************************************************************
    !*****************************************************************************************

        Interface
            FUNCTION fx(x_0, t_0)
                integer, parameter          :: dp=kind(0.d0)
                REAL(dp), INTENT(IN)        :: x_0(6), t_0
                REAL(dp), dimension(6)      :: fx
            END FUNCTION fx
        END INTERFACE

        integer, parameter                              :: dp=kind(0.d0)
        real(dp), external, optional                    :: events
        integer                                         :: I, max_eval, j, k, jj
        real(dp), intent(in)                            :: x_0(6), t_0, t_f, err_min, hmin, hs, hmax
        real(dp), intent(out), optional, allocatable    :: x_out_events(:,:), t_out_events(:)
        real(dp)                                        :: t_out, h, t, RFNORM, errest, errnorm, &
                                                            err_max, t_1, scale_h, t_events
        real(dp), dimension(6)                          :: x_out, x, x_temp, err_out, x_1, temp, weights, x_events
        real(dp), allocatable                           :: x_all(:,:), t_all(:)
        real(dp), target, allocatable                   :: a_temp(:,:), t_temp(:), xe_temp(:,:), tt_temp(:)
        logical                                         :: last_step, cond, file_chk
        logical, optional                               :: terminal
        data file_chk /.false./

        if (file_chk .eqv. .false.) then
            call file_open("RK_err_log.dat", 100)
            file_chk = .true.
        else
            open(Unit = 100, File = "RK_err_log.dat", form = 'Formatted', status = "Old",&
                position="append", Action = "Write")
        end if
        last_step   = .false.
        cond        = .false.
        H           = ABS(HS)
        jj          = 0

        if (int(t_f-t_0) .eq. 0)then
            allocate(a_temp(100, 6));          allocate(t_temp(100))
        elseif (int(t_f-t_0) .gt. 100) then
            allocate(a_temp((int(t_f-t_0)*20), 6));          allocate(t_temp(int(t_f-t_0)*20))
        else
            allocate(a_temp((int(t_f-t_0)*100), 6));          allocate(t_temp(int(t_f-t_0)*100))
        end if
        if (present(events)) then
            allocate(xe_temp(int(t_f-t_0), 6));    allocate(tt_temp(int(t_f-t_0)))
        end if

!        x_allocator => a_temp;          t_allocator => t_temp
!        allocate(x_allocator);              allocate(t_allocator)

        a_temp = 0.0_dp;                t = t_0;
        RFNORM = 0.0_dp;
        errest = 1.0_dp/7.0_dp;         max_eval = 15
        x_1 = x_0;                      t_1 = t_0

        if (abs(h) .gt. abs(t_f - t_1)) then
            H = (t_f - t_1)
            last_step = .true.
        elseif (h+ 0.5*h .gt. abs(t_f - t_1)) then
            h = 0.50_dp*h
        end if
        weights = abs(x_1)
        j = 0;      i = 0
        jj = 0;

        do while(abs(t) .lt. abs(t_f))
            j = j+1
            do i = 1, max_eval
                x_temp = x_1
                call RK_tab (fx, x_1, t_1, h, err_out)
                write(100, *)(err_out(k), k=1,6)

                if (j .gt. 1)then
                    do k = 1, 6
                        weights(k) = maxab(weights(k), abs(x_1(k)))
                    end do
                    temp = err_out/(weights)
                else
                    temp = err_out
                end if
                errnorm = norm2(temp)

                RFNORM = (err_min/(2.0_dp*errnorm))** errest

                if (errnorm .le. err_min)then
                    if (RFNORM .ge. 2.0_dp)then
                        scale_h = 2.00_dp
                    elseif(RFNORM .ge. 1.0_dp .and. RFNORM .lt. 2.0_dp)then
                        scale_h = RFNORM**(0.9)
                    end if
                    a_temp(j,:) = x_1(1:6)
                    t_temp(j)   = t_1
!                    print *, t_1
                    exit

                elseif(errnorm .lt. err_max)then
                    scale_h = 0.90_dp
                    h = h*scale_h
                    x_1 = x_temp
                else
                    scale_h = maxab(0.50_dp, minab(0.90_dp, RFNORM))
                    h = h*scale_h
                    x_1 = x_temp
                    if (i .gt. 2)then
                        print *, 'Finding optimal Step Length...'
                    end if
                end if

            end do

            if (i .gt. max_eval) then
                print *, 'Cannot converge, Exiting Code...!'
                return
            end if

            if (present(events)) then
                if (events(x, t_1)*events(x_1, t_1+h) .lt. 0.0_dp)then
!                    print*, 'Calling events Function'
                    call eval_events(events, fx, x, x_1, t_1, t_1+h, x_events, t_events)
                    jj = jj+1
                    xe_temp(jj,:) = x_events(1:6);      tt_temp(jj) = t_events
!                    print*, 'Exiting Events Function'
                end if
            end if

            x = x_1;        t_1 = t_1+h
            h = h*scale_h
            if (h .gt. hmax)then
                h = hmax
            end if

            if (last_step) then
                exit
            elseif(cond .eqv. .true.)then
                exit
            end if


            if (abs(h) .gt. abs(t_f - t_1)) then
                H = (t_f - t_1)
                last_step = .true.
            elseif (t_1+h+ 0.5*h .gt. t_f) then
                h = 0.50_dp*h
            end if

        end do
        x_all = a_temp(1:j, :);         t_all = t_temp(1:j)

        x_out = x_1;        t_out = t_1
        close(100)

        if (present(events)) then
            x_out_events = xe_temp(1:jj,:);     t_out_events = tt_temp(1:jj)
            deallocate(xe_temp);                deallocate(tt_temp)
        end if

        deallocate(a_temp);

    endsubroutine

    subroutine RK_tab (fx, x, t, h, err_out)

        INTERFACE
            FUNCTION fx(x_0, t_0)
                integer, parameter          :: dp=kind(0.d0)
                REAL(dp), INTENT(IN)        :: x_0(6), t_0
                REAL(dp), dimension(6)      :: fx
            END FUNCTION fx
        END INTERFACE
        integer, parameter      :: dp=kind(0.d0)
        real(dp), intent(inout) :: t, h, x(6), err_out(6)
        real(dp), parameter     ::  c_1_11 = 41.0_dp / 840.0_dp, c6 = 34.0_dp / 105.0_dp,&
                                    c_7_8= 9.0_dp / 35.0_dp, c_9_10 = 9.0_dp / 280.0_dp

        real(dp), parameter     ::  a2 = 2.0_dp / 27.0_dp, a3 = 1.0_dp / 9.0_dp, a4 = 1.0_dp / 6.0_dp,  &
                                    a5 = 5.0_dp / 12.0_dp, a6 = 1.0_dp / 2.0_dp,  a7 = 5.0_dp / 6.0_dp, &
                                    a8 = 1.0_dp / 6.0_dp, a9 = 2.0_dp / 3.0_dp, a10 = 1.0_dp / 3.0_dp

        real(dp), parameter     ::  b31 = 1.0_dp / 36.0_dp, b32 = 3.0_dp / 36.0_dp, b41 = 1.0_dp / 24.0_dp, &
                                    b43 = 3.0_dp / 24.0_dp, b51 = 20.0_dp / 48.0_dp, b53 = -75.0_dp / 48.0_dp, &
                                    b54 = 75.0_dp / 48.0_dp, b61 = 1.0_dp / 20.0_dp, b64 = 5.0_dp / 20.0_dp, b65 = 4.0_dp / 20.0_dp

        real(dp), parameter     ::  b71 = -25.0_dp / 108.0_dp, b74 =  125.0_dp / 108.0_dp, b75 = -260.0_dp / 108.0_dp, &
                                    b76 =  250.0_dp / 108.0_dp, b81 = 31.0_dp/300.0_dp, b85 = 61.0_dp/225.0_dp, &
                                    b86 = -2.0_dp/9.0_dp, b87 = 13.0_dp/900.0_dp, b91 = 2.0_dp, b94 = -53.0_dp/6.0_dp,&
                                    b95 = 704.0_dp/45.0_dp, b96 = -107.0_dp / 9.0_dp, b97 = 67.0_dp / 90.0_dp, b98 = 3.0_dp

        real(dp), parameter     ::  b10_1 = -91.0_dp / 108.0_dp, b10_4 = 23.0_dp / 108.0_dp, b10_5 = -976.0_dp / 135.0_dp, &
                                    b10_6 = 311.0_dp / 54.0_dp, b10_7 = -19.0_dp / 60.0_dp, b10_8 = 17.0_dp / 6.0_dp, &
                                    b10_9 = -1.0_dp / 12.0_dp, b11_6 = -301.0_dp / 82.0_dp

        real(dp), parameter     ::  b11_1 = 2383.0_dp / 4100.0_dp, b11_4 = -341.0_dp / 164.0_dp, b11_5 = 4496.0_dp / 1025.0_dp, &
                                    b11_7 = 2133.0_dp / 4100.0_dp, b11_8 = 45.0_dp / 82.0_dp, b11_9 = 45.0_dp / 164.0_dp,&
                                    b11_10 = 18.0_dp / 41.0_dp

        real(dp), parameter     ::  b12_1 = 3.0_dp / 205.0_dp, b12_6 = - 6.0_dp / 41.0_dp, b12_7 = - 3.0_dp / 205.0_dp, &
                                    b12_8 = - 3.0_dp / 41.0_dp, b12_9 = 3.0_dp / 41.0_dp, b12_10 = 6.0_dp / 41.0_dp, &
                                    b13_1 = -1777.0_dp / 4100.0_dp

        real(dp), parameter     ::  b13_4 = -341.0_dp / 164.0_dp, b13_5 = 4496.0_dp / 1025.0_dp, b13_6 = -289.0_dp / 82.0_dp, &
                                    b13_7 = 2193.0_dp / 4100.0_dp, b13_8 = 51.0_dp / 82.0_dp, b13_9 = 33.0_dp / 164.0_dp,&
                                    b13_10 = 12.0_dp / 41.0_dp

        real(dp), parameter     ::  err_factor  = -41.0_dp / 840.0_dp;

        real(dp), dimension(6)  ::  k1, k2, k3, k4, k5, k6, k7, k8, k9, k10, k11, k12, k13
        real(dp)                ::  h2_7

        h2_7 = a2 * h

        k1 = fx(x, t);
        k2 = fx(x + h2_7 * k1, t+h2_7)
        k3 = fx(x + h * ( b31*k1 + b32*k2), t+a3*h );
        k4 = fx(x + h * ( b41*k1 + b43*k3), t+a4*h );
        k5 = fx(x + h * ( b51*k1 + b53*k3 + b54*k4), t+a5*h );
        k6 = fx(x + h * ( b61*k1 + b64*k4 + b65*k5), t+a6*h );
        k7 = fx(x + h * ( b71*k1 + b74*k4 + b75*k5 + b76*k6), t+a7*h );
        k8 = fx(x + h * ( b81*k1 + b85*k5 + b86*k6 + b87*k7), t+a8*h );
        k9 = fx(x + h * ( b91*k1 + b94*k4 + b95*k5 + b96*k6 + b97*k7 + b98*k8), t+a9*h );
        k10 = fx(x + h * ( b10_1*k1 + b10_4*k4 + b10_5*k5 + b10_6*k6 &
                          + b10_7*k7 + b10_8*k8 + b10_9*k9 ), t+a10*h );
        k11 = fx(x + h * ( b11_1*k1 + b11_4*k4 + b11_5*k5 + b11_6*k6 &
                           + b11_7*k7 + b11_8*k8 + b11_9*k9 + b11_10 * k10 ), t+h );
        k12 = fx(x + h * ( b12_1*k1 + b12_6*k6 + b12_7*k7 + b12_8*k8&
                            + b12_9*k9 + b12_10 * k10 ), t );
        k13 = fx(x + h * ( b13_1*k1 + b13_4*k4 + b13_5*k5 + b13_6*k6 &
                            + b13_7*k7 + b13_8*k8 + b13_9*k9 + b13_10*k10 + k12 ), t+h );
        x = x +  h * (c_1_11 * (k1 + k11)  + c6 * k6 + c_7_8 * (k7 + k8)&
                                           + c_9_10 * (k9 + k10) )
!        t = t+h

        err_out = err_factor*(k1+k11-k12)
        err_out = h*(err_out - err_factor*k13)

    end subroutine

    subroutine eval_events(terminalfun, fx, y_1, y_2, t_1, t_2, x_out, t_out, isterminal)
        interface
            function terminalfun(x, t)
                integer, parameter                  :: dp = kind(0.d0)
                real(dp), dimension(6), intent(in)  :: x
                real(dp), intent(in)                :: t
                real(dp)                            :: terminalfun
            end function
        end interface

        Interface
            FUNCTION fx(x_0, t_0)
                integer, parameter          :: dp=kind(0.d0)
                REAL(dp), INTENT(IN)        :: x_0(6), t_0
                REAL(dp), dimension(6)      :: fx
            END FUNCTION fx
        END INTERFACE

        integer, parameter                  :: dp = kind(0.d0)
        real(dp), dimension(6), intent(in)  :: y_1, y_2
        real(dp), intent(in)                :: t_1, t_2
        real(dp)                            :: t_out, event_eval_i, event_eval_j, err_min,&
                                               event_eval_new, t_i, t_j, t_new, slope
        real(dp), dimension(6)              :: x_out, y_i, y_new, y_j
        real(dp), allocatable               :: x_all(:,:), t_all(:)
        integer                             :: k
        logical, optional                   :: isterminal
        logical                             :: cond

        cond = .false.
        k = 0
        err_min = 1d-7
        y_i = y_1;              t_i = t_1
        y_j = y_2;              t_j = t_2

        do while (cond .eqv. .False.)
            event_eval_i = terminalfun(y_i, t_i)
            event_eval_j = terminalfun(y_j, t_j)
            if (t_j - t_i .gt. err_min) then
                slope = (event_eval_i - event_eval_j)/(t_i - t_j)
                t_new = t_i - event_eval_i/slope
                call RKFcall(fx, y_i, t_i, t_j, y_new, t_new, x_all, t_all)
                event_eval_new = terminalfun(y_new, t_new)
                if (event_eval_new*event_eval_j .gt. 0.0_dp) then
                    y_i = y_new
                    t_i = t_new
                else
                    y_j = y_new
                    t_j = t_new
                end if
            else
                cond = .True.
            end if

            k = k+1
            if (k .gt. 100) then
                print*, 'Exceeded maximum number of iterations'
                cond = .true.
            end if
        end do
        x_out = y_new
        t_out = t_new
    end subroutine


end module
