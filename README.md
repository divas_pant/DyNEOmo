# Dynamical Near Earth Model (DyNEOmo)

Is a Fortran based project aimed at studying the dynamics of Near Earth Objects using various numerical methods.

## Installation

Program is compiled using GNU Fortran compiler in codeblocks and tested with same compiler on Ubuntu based system, and should work with any standard fortran compiler.
For a linux system run makefile

## Usage
After compiling keep JPLEPH and Neodys.ctc in the same folder as executable.

** The program reads NEODys data file for asteroid and generates desired number of data points using Monte-Carlo method and integrate each one till given desired time. The output contains initial and final state vector for each sample generated.
** If more than 1 MC samples are integrated then the data is added to each file in the order of each sample.
** Dynamics of NEO can also be evaluated for a given time frame, by selecting the right option.The output is the vector of state variables for the entire period, along with the Keplerian parameters for the same.

*** Based on the initial selection, output data is sorted into separate folders named according to the selection. Output files are formatted files that can be viewed in any text editor.

All computations are done using double precision.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[MIT](https://choosealicense.com/licenses/mit/)
