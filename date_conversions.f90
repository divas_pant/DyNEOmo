module date_conversions
    implicit none
    contains

    subroutine sidereal_time(date_in, time_in, EL, LST)
        integer, parameter :: dp=kind(0.d0)
        real(dp), intent(in), dimension(3) :: time_in, date_in
        real(dp), intent(in) :: EL
        real(dp) :: jd, jj, g0, gst, ut, LST

        jd = date2jd(date_in)
        jj = (jd - 2451545)/36525

        g0 = 100.4606184 + 36000.77004*jj + 0.000387933*(jj**2) - 2.583e-8*(jj**3)
        if (g0 >= 360) then
            g0 = g0 - floor(g0/360)*360
        elseif (g0 < 0.0) then
            g0 = g0 - (-floor(abs(g0/360)) - 1)*360
        end if

        call time2utc(time_in, ut)

        gst = g0 + 360.98564724*ut/24

        lst = gst + EL
        lst = lst - 360*floor(lst/360)

    end subroutine

    function date2jd(date_in, time_in)
        integer, parameter :: dp=kind(0.d0)
        real(dp), intent(in), dimension(3) :: date_in
        real(dp), optional, dimension(3) :: time_in
        real(dp) :: month, day, year, utc, date2jd
        real(dp) :: jd

        year = date_in(3);  month = date_in(2);  day = date_in(1)

        if(present(time_in)) then
            call time2utc(time_in, utc)
        else
            utc = 0
        end if


        jd = 367*year - floor(7*(year + floor((month + 9)/12))/4) + floor(275*month/9) + day + 1721013.5
        date2jd = jd + utc/24

    end function

    function mjd2jd(mjd)
        integer, parameter      :: dp=kind(0.d0)
        real(dp), intent(in)    :: mjd
        real(dp)                :: mjd2jd
        mjd2jd = mjd +2400000.5
    end function

    subroutine time2utc(time_in, utc)
        integer, parameter :: dp=kind(0.d0)
        real(dp), intent(in), dimension(3) :: time_in
        real(dp) :: hour, minutes, seconds, utc

        hour = time_in(1);  minutes = time_in(2);   seconds = time_in(3)

        utc = hour + minutes/60.0 + seconds/3600
    end subroutine



    function jed2mjd2000(jed)
        integer, parameter      :: dp=kind(0.d0)
        real(dp), intent(in)    :: jed
        real(dp)                :: jed2mjd2000
        jed2mjd2000 = jed-2451544.5
    end function

end module
